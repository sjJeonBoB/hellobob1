import unittest
import random
import app

class TestFlask(unittest.TestCase):
    a = random.randint(-10000, 10000)
    b = random.randint(-10000, 10000)
    add = a+b
    sub = a-b
    def setUp(self):
        self.app = app

    def test_add(self): 

        query = app.app.test_client().get('/add?a={}&b={}'.format(self.a, self.b))
        self.assertEqual(self.add, int(query.get_data()))

    def test_sub(self):

        query = app.app.test_client().get('/sub?a={}&b={}'.format(self.a, self.b))
        self.assertEqual(self.sub, int(query.get_data()))

if __name__ == "__main__":
    unittest.main()
